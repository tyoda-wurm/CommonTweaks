# CommonTweaks

This mod will, in the future, add an assortment of stuff to the game.

This is a server mod.

# Features
## Reload Config
 - The config of this mod can be reloaded at any time by anyone at/above the configurable gm level.
 - Whole modules can not be loaded/unloaded this way, only the options inside each module.

## Sound timer module.
 - Fix sounds not playing when the player is completing the action too quickly. This way a certain sound will play when an action is started, and then every 5 seconds, regardless of how many actions were completed in that time.
 - Inspired by <a href="https://forum.wurmonline.com/index.php?/topic/190633-playing-sound-on-action-timer-tick/">this post.</a>

## Adjust Render Distance module
 - Adjust the render distance of individual items
 - Make every unfinished item render to the same distance as its finished counterpart
 
## Make items droppable
 - Make any item droppable
 - Hand mirrors droppable by default (if module enabled)

## Miscellaneous
 - Make rift jewelry enchantable
