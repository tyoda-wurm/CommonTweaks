/*
CommonTweaks mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License v3 as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.CommonTweaks;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.items.*;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.VirtualZone;
import com.wurmonline.server.zones.VolaTile;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.*;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.tyoda.wurm.CommonTweaks.actions.ReloadConfigAction;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;

public class CommonTweaks implements WurmServerMod, Configurable, PreInitable, Initable, ServerStartedListener, PlayerLoginListener, ItemTemplatesCreatedListener {
    public static final Logger logger = Logger.getLogger(CommonTweaks.class.getName());
    public static final String version = "0.8";
    public static final Path configPath = Paths.get("mods/CommonTweaks.config");


    // Sound Timer module
    private boolean soundTimerModule = false;
    private int soundMillis = 5000;
    private final HashMap<Long, HashMap<String, Long>> soundData = new HashMap<>();

    // Adjust render distance module
    private boolean adjustRenderDistanceModule = false;

    private boolean alwaysAdjustUnfinished = false;

    private final HashMap<Integer, Integer> adjustRenderMap = new HashMap<>();

    // Make items droppable
    private boolean makeItemsDroppableModule = false;
    private final ArrayList<Integer> droppableItems = new ArrayList<>();

    // Animal Names
    private boolean animalNamesModule = false;
    private boolean useOnlyOneName = false;
    private boolean allCreaturesCustomNames = false;
    private boolean clearMaleNames = false;
    private boolean clearFemaleNames = false;
    private boolean clearGenericNames = false;
    private boolean clearMaleUnicornNames = false;
    private boolean clearFemaleUnicornNames = false;
    private final ArrayList<String> customMaleNames = new ArrayList<>();
    private final ArrayList<String> customFemaleNames = new ArrayList<>();
    private final ArrayList<String> customGenericNames = new ArrayList<>();
    private final ArrayList<String> customMaleUnicornNames = new ArrayList<>();
    private final ArrayList<String> customFemaleUnicornNames = new ArrayList<>();

    // Craftable Merchant
    private boolean craftContract = false;
    private boolean craftFiveTwenty = true;
    private int contractPrice = 10;
    private float contractDifficulty = 20.0f;

    // Miscellaneous
    private boolean miscellaneousModule = false;
    private static final int[] riftJewelries = new int[]{1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090};
    private boolean makeRiftJewelryEnchantable = false;
    private final ArrayList<Integer> notRebirthables = new ArrayList<>();

    // other stuff
    private boolean loadedConfig = false;
    private int reloadGmPower = 4;
    private static CommonTweaks instance;

    public CommonTweaks(){
        instance = this;
    }

    @Override
    public void configure(Properties p) {
        logger.info("Starting configure");
        doConfig(p);
        loadedConfig = true;
        logger.info("Done with configure");
    }

    public void doConfig(Properties p){
        reloadGmPower = Integer.parseInt(p.getProperty("reloadGMPower", String.valueOf(reloadGmPower)));

        // Sound Timer module
        if(!loadedConfig)
            soundTimerModule = Boolean.parseBoolean(p.getProperty("soundTimerModule", String.valueOf(soundTimerModule)));
        if(soundTimerModule) {
            int pSoundMillis = Integer.parseInt(p.getProperty("sTMillis", String.valueOf(soundMillis)));
            if (pSoundMillis > 0) soundMillis = pSoundMillis;
        }

        // Adjust render distance module
        if(!loadedConfig)
            adjustRenderDistanceModule = Boolean.parseBoolean(p.getProperty("enableAdjustRenderDistance", String.valueOf(adjustRenderDistanceModule)));
        if(adjustRenderDistanceModule){
            alwaysAdjustUnfinished = Boolean.parseBoolean(p.getProperty("aRDAlwaysAdjustUnfinished", String.valueOf(alwaysAdjustUnfinished)));

            adjustRenderMap.clear();

            String[] adjustRenders = p.getProperty("aRDItems", "").trim().split(",");
            for(String adjustRender : adjustRenders){
                if(adjustRender.trim().equals("")) continue;

                String[] keyValue = adjustRender.split(";");
                adjustRenderMap.put(Integer.valueOf(keyValue[0].trim()), Integer.valueOf(keyValue[1].trim()));
                logger.info("Adjusted item id "+keyValue[0].trim()+" render distance to "+keyValue[1].trim());
            }
        }

        // Make items droppable
        if(!loadedConfig){
            makeItemsDroppableModule = Boolean.parseBoolean(p.getProperty("enableMakeItemsDroppable", String.valueOf(makeItemsDroppableModule)));
            if(makeItemsDroppableModule) {
                String[] droppableItemStrings = p.getProperty("midDroppableItems", "781, 600").split(",");
                for (String droppableItemString : droppableItemStrings) {
                    if (droppableItemString.trim().equals("")) continue;
                    droppableItems.add(Integer.parseInt(droppableItemString.trim()));
                }
            }
        }

        // Animal Names
        if(!loadedConfig) {
            animalNamesModule = Boolean.parseBoolean(p.getProperty("enableAnimalNames", String.valueOf(animalNamesModule)));
            if(animalNamesModule) {
                allCreaturesCustomNames = Boolean.parseBoolean(p.getProperty("anAllCreaturesCustomNames", String.valueOf(allCreaturesCustomNames)));
                useOnlyOneName = Boolean.parseBoolean(p.getProperty("anUseOnlyOneName", String.valueOf(useOnlyOneName)));
                clearMaleNames = Boolean.parseBoolean(p.getProperty("anClearMaleNames", String.valueOf(clearMaleNames)));
                clearFemaleNames = Boolean.parseBoolean(p.getProperty("anClearFemaleNames", String.valueOf(clearFemaleNames)));
                clearGenericNames = Boolean.parseBoolean(p.getProperty("anClearGenericNames", String.valueOf(clearGenericNames)));
                clearMaleUnicornNames = Boolean.parseBoolean(p.getProperty("anClearMaleUnicornNames", String.valueOf(clearMaleUnicornNames)));
                clearFemaleUnicornNames = Boolean.parseBoolean(p.getProperty("anClearFemaleUnicornNames", String.valueOf(clearFemaleUnicornNames)));
                String[] maleNames = p.getProperty("anCustomMaleNames", "").split(",");
                addNonEmptyCustomNames(maleNames, customMaleNames);
                String[] femaleNames = p.getProperty("anCustomFemaleNames", "").split(",");
                addNonEmptyCustomNames(femaleNames, customFemaleNames);
                String[] genericNames = p.getProperty("anCustomGenericNames", "").split(",");
                addNonEmptyCustomNames(genericNames, customGenericNames);
                String[] maleUnicornNames = p.getProperty("anCustomMaleUnicornNames", "").split(",");
                addNonEmptyCustomNames(maleUnicornNames, customMaleUnicornNames);
                String[] femaleUnicornNames = p.getProperty("anCustomFemaleUnicornNames", "").split(",");
                addNonEmptyCustomNames(femaleUnicornNames, customFemaleUnicornNames);
            }
        }

        // Craftable Merchant
        if(!loadedConfig) {
            craftContract = Boolean.parseBoolean(p.getProperty("cmCraftContract", String.valueOf(craftContract)));
            if(craftContract) {
                contractDifficulty = Float.parseFloat(p.getProperty("cmContractDifficulty", String.valueOf(contractDifficulty)));
                craftFiveTwenty = Boolean.parseBoolean(p.getProperty("cmCraftFiveTwenty", String.valueOf(craftFiveTwenty)));
                contractPrice = Integer.parseInt(p.getProperty("cmContractPrice", String.valueOf(contractPrice)));
                if(contractPrice < 0) {
                    logger.warning("Contract price was set to lower than 0: "+contractPrice+". Using 0.");
                    contractPrice = 0;
                }
            }
        }

        // Miscellaneous
        if(!loadedConfig){
            miscellaneousModule = Boolean.parseBoolean(p.getProperty("enableMiscellaneous", String.valueOf(miscellaneousModule)));
            makeRiftJewelryEnchantable = Boolean.parseBoolean(p.getProperty("miscRiftEnchantable", String.valueOf(makeRiftJewelryEnchantable)));
            final String[] notRebirthableStrings = p.getProperty("makeNotRebirthable", "").split(",");
            for (String notRebirthableString : notRebirthableStrings) {
                notRebirthableString = notRebirthableString.trim();
                if(notRebirthableString.equals("")) continue;
                notRebirthables.add(Integer.parseInt(notRebirthableString));
            }
        }
    }

    private void addNonEmptyCustomNames(String[] names, ArrayList<String> array){
        for (String name : names) {
            String trimmed = name.trim();
            if (trimmed.equals("")) continue;
            array.add(trimmed);
        }
    }

    @Override
    public void preInit() {
        ModActions.init();
        try {
            if(makeItemsDroppableModule){
                logger.info("Make Items Droppable module");
                for (int templateId : droppableItems){
                    logger.info("Item with id "+templateId+" will be droppable.");
                    makeDroppable(templateId);
                }
                droppableItems.clear();
            }

            if(animalNamesModule) {
                logger.info("Animal Names module.");
                animalNamesPreInit();
            }
            customMaleNames.clear();
            customFemaleNames.clear();
            customGenericNames.clear();
            customMaleUnicornNames.clear();
            customFemaleUnicornNames.clear();

            if(miscellaneousModule){
                logger.info("Miscellaneous module");
                if(makeRiftJewelryEnchantable){
                    for(int templateId : riftJewelries){
                        logger.info("Item with id "+templateId+" will be enchantable.");
                        makeEnchantable(templateId);
                    }
                }
                if(!notRebirthables.isEmpty()) {
                    logger.info(notRebirthables.size() + " creature" + (notRebirthables.size() > 1 ? "s" : "") + " will not be rebirthable.");
                    makeNotRebirthable();
                }
            }
        } catch (NotFoundException | CannotCompileException e){
            throw new HookException(e);
        }
    }

    private void animalNamesPreInit() throws NotFoundException, CannotCompileException {
        ClassPool classPool = HookManager.getInstance().getClassPool();

        if(useOnlyOneName) {
            CtClass ctOffspring = classPool.getCtClass("com.wurmonline.server.creatures.Offspring");
            ctOffspring.getDeclaredMethod("generateMaleName")
                .setBody("return MALE_NAMES.length > 0 ? MALE_NAMES[com.wurmonline.server.Server.rand.nextInt(MALE_NAMES.length)] : \"\";");
            ctOffspring.getDeclaredMethod("generateFemaleName")
                .setBody("return FEMALE_NAMES.length > 0 ? FEMALE_NAMES[com.wurmonline.server.Server.rand.nextInt(FEMALE_NAMES.length)] : \"\";");
            ctOffspring.getDeclaredMethod("generateGenericName")
                .setBody("return GENERIC_NAMES.length > 0 ? GENERIC_NAMES[com.wurmonline.server.Server.rand.nextInt(GENERIC_NAMES.length)] : \"\";");
            ctOffspring.getDeclaredMethod("generateFemaleUnicornName")
                .setBody("return FEMALE_UNI_NAMES.length > 0 ? FEMALE_UNI_NAMES[com.wurmonline.server.Server.rand.nextInt(FEMALE_UNI_NAMES.length)] : \"\";");
            ctOffspring.getDeclaredMethod("generateMaleUnicornName")
                .setBody("return MALE_UNI_NAMES.length > 0 ? MALE_UNI_NAMES[com.wurmonline.server.Server.rand.nextInt(MALE_UNI_NAMES.length)] : \"\";");
        }

        if(allCreaturesCustomNames) {
            CtClass ctCreature = classPool.getCtClass("com.wurmonline.server.creatures.Creature");
            ctCreature.getDeclaredMethod("checkPregnancy", new CtClass[]{CtClass.booleanType})
                    .instrument(new ExprEditor(){
                        @Override
                        public void edit(MethodCall m) throws CannotCompileException {
                            if(m.getMethodName().equals("isHorse")) {
                                m.replace("$_ = true;");
                            }
                        }
                    });
        }

        if(customMaleNames.size() > 0) {
            animalNamesPreInitHelper("malelist", clearMaleNames, customMaleNames, "createMaleNames");
        }
        if(customFemaleNames.size() > 0) {
            animalNamesPreInitHelper("femalelist", clearFemaleNames, customFemaleNames, "createFemaleNames");
        }
        if(customGenericNames.size() > 0) {
            animalNamesPreInitHelper("genericlist", clearGenericNames, customGenericNames, "createGenericNames");
        }
        if(customMaleUnicornNames.size() > 0) {
            animalNamesPreInitHelper("mul", clearMaleUnicornNames, customMaleUnicornNames, "createMaleUnicornNames");
        }
        if(customFemaleUnicornNames.size() > 0) {
            animalNamesPreInitHelper("ful", clearFemaleUnicornNames, customFemaleUnicornNames, "createFemaleUnicornNames");
        }
    }

    private static void animalNamesPreInitHelper(String arrName, boolean clearNames, ArrayList<String> names, String methodName) throws CannotCompileException, NotFoundException {
        ClassPool classPool = HookManager.getInstance().getClassPool();
        CtClass ctOffspring = classPool.getCtClass("com.wurmonline.server.creatures.Offspring");
        StringBuilder source = new StringBuilder(clearNames ? arrName+".clear();" : "");
        for (String name : names)
            source.append(arrName).append(".add(\"").append(name).append("\");");
        ctOffspring.getDeclaredMethod(methodName).instrument(new ExprEditor(){
            @Override
            public void edit(MethodCall m) throws CannotCompileException {
                if(m.getMethodName().equals("toArray")) {
                    m.replace(source+"; $_ = "+arrName+".toArray(new String[0]);");
                }
            }
        });
    }

    private static void makeEnchantable(int templateId) throws NotFoundException, CannotCompileException {
        ClassPool classPool = HookManager.getInstance().getClassPool();
        CtClass ctItemTemplateFactory = classPool.getCtClass("com.wurmonline.server.items.ItemTemplateFactory");
        ctItemTemplateFactory.getDeclaredMethod("createItemTemplate").insertBefore(""+
                "if(templateId == "+templateId+"){" +
                    "short[] newItemTypes = new short[itemTypes.length+1];" +
                    "System.arraycopy(itemTypes, 0, newItemTypes, 0, itemTypes.length);" +
                    "newItemTypes[itemTypes.length] = (short)153;" +
                    "itemTypes = newItemTypes;" +
                "}"
        );
    }

    private void makeNotRebirthable() throws NotFoundException, CannotCompileException {
        ClassPool classPool = HookManager.getInstance().getClassPool();
        CtClass ctCreatureTemplate = classPool.getCtClass("com.wurmonline.server.creatures.CreatureTemplate");
        ctCreatureTemplate.getConstructors()[0].insertAfter("this.isNotRebirthable = this.isNotRebirthable || org.tyoda.wurm.CommonTweaks.CommonTweaks.isNotRebirthable($1);");
    }

    private static void makeDroppable(int templateId) throws NotFoundException, CannotCompileException {
        ClassPool classPool = HookManager.getInstance().getClassPool();
        CtClass ctItemTemplateFactory = classPool.getCtClass("com.wurmonline.server.items.ItemTemplateFactory");
        ctItemTemplateFactory.getDeclaredMethod("createItemTemplate").insertBefore(""+
                "if(templateId == "+templateId+"){" +
                    "boolean found = false;" +
                    "for(int i = 0; i < itemTypes.length; ++i){" +
                        "if(itemTypes[i] == 42){found = true; break;}" +
                    "}" +
                    "if(found){" +
                        "short[] newItemTypes = new short[itemTypes.length - 1];" +
                        "int cursor = 0;" +
                        "for(int i = 0; i < itemTypes.length; ++i){" +
                            "if(itemTypes[i] == 42) continue;" +
                            "newItemTypes[cursor++] = itemTypes[i];" +
                        "}" +
                        "itemTypes = newItemTypes;" +
                    "}" +
                "}"
        );
    }

    @Override
    public void init() {
        logger.info("Starting init");

        HookManager hM = HookManager.getInstance();

        // Sound Timer module
        if(soundTimerModule) {
            logger.info("Injecting Sound Timer module");
            hM.registerHook(
                "com.wurmonline.server.behaviours.Action",
                "mayPlaySound",
                "()Z",
                () ->
                    (proxy, method, args) -> {
                        Action action = (Action)proxy;
                        if(action.getPerformer().isPlayer()){
                            String actionString = action.getActionString();
                            long currentMillis = System.currentTimeMillis();

                            HashMap<String, Long> playerData = soundData.get(action.getPerformer().getWurmId());
                            // Create entry for action sound for player if absent
                            playerData.putIfAbsent(actionString, 0L);

                            // see if 5 seconds have passed since last sound was played
                            boolean playSound = currentMillis >= playerData.get(actionString) + soundMillis;
                            if(playSound)
                                playerData.put(actionString, currentMillis);

                            return playSound;
                        }else{
                            return action.currentSecond() % 5 == 0;
                        }
            });
            logger.info("Successfully injected Sound Timer module");
        }else logger.info("Skipping Sound Timer module.");

        // Unfinished render distance

        if(adjustRenderDistanceModule){
            logger.info("Injecting Adjust Render Distance module");
            hM.registerHook(
                    "com.wurmonline.server.zones.VirtualZone",
                    "isVisible",
                    "(Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/zones/VolaTile;)Z",
                    () ->
                        (proxy, method, args) -> {
                            final int templateId = ((Item)args[0]).getTemplateId();

                            Integer adjusted = adjustRenderMap.get(templateId);
                            if(templateId == 179){
                                final int finishedTemplateId = AdvancedCreationEntry.getTemplateId((Item) args[0]);
                                adjusted = adjustRenderMap.get(finishedTemplateId);
                                if(alwaysAdjustUnfinished && adjusted == null){
                                    int iSize;
                                    try {
                                        iSize = ItemTemplateFactory.getInstance().getTemplate(AdvancedCreationEntry.getTemplateId((Item) args[0])).getSizeZ();
                                    } catch(NoSuchTemplateException e){
                                        return method.invoke(proxy, args);
                                    }
                                    adjusted = 3;
                                    if(iSize >= 500){
                                        return true;
                                    } else if (iSize >= 300) {
                                        adjusted = 128;
                                    } else if (iSize >= 200) {
                                        adjusted = 64;
                                    } else if (iSize >= 100) {
                                        adjusted = 32;
                                    } else if (iSize >= 50) {
                                        adjusted = 16;
                                    } else if (iSize >= 10) {
                                        adjusted = 8;
                                    }
                                }
                            }

                            if(adjusted != null){
                                VolaTile tile = (VolaTile) args[1];
                                int distanceX = Math.abs(tile.getTileX() - ((VirtualZone)proxy).getCenterX());
                                int distanceY = Math.abs(tile.getTileY() - ((VirtualZone)proxy).getCenterY());
                                return Math.max(distanceX, distanceY) <= adjusted;
                            }else{
                                return method.invoke(proxy, args);
                            }
                        }
            );
            logger.info("Successfully injected Adjust Render Distance module");
        }

        logger.info("Done with init");
    }

    @Override
    public void onItemTemplatesCreated() {
        logger.info("Starting onItemTemplatesCreated.");
        if(craftContract) {
            try {
                ItemTemplate contractTemplate = ItemTemplateFactory.getInstance().getTemplate(ItemList.merchantContract);
                ReflectionUtil.setPrivateField(contractTemplate, ReflectionUtil.getField(ItemTemplate.class, "difficulty"), contractDifficulty);
            } catch (NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
                throw new HookException(e);
            }
            if(contractPrice > 0) {
                AdvancedCreationEntry contractCreation = CreationEntryCreator.createAdvancedEntry(SkillList.PAPYRUSMAKING, ItemList.paperSheet, ItemList.ink, ItemList.merchantContract, true, true, 0.1f, true, false, CreationCategories.WRITING);
                contractCreation.addRequirement(new CreationRequirement(1, ItemList.coinSilver, contractPrice, true));
                logger.info("Added creation entry for one-silver coins.");
                if(craftFiveTwenty && contractPrice % 5 == 0) {
                    AdvancedCreationEntry contractCreationFive = CreationEntryCreator.createAdvancedEntry(SkillList.PAPYRUSMAKING, ItemList.paperSheet, ItemList.ink, ItemList.merchantContract, true, true, 0.1f, true, false, CreationCategories.WRITING);
                    contractCreationFive.addRequirement(new CreationRequirement(1, ItemList.coinSilverFive, contractPrice/5, true));
                    logger.info("Added creation entry for five-silver coins.");
                }
                if(craftFiveTwenty && contractPrice % 20 == 0) {
                    AdvancedCreationEntry contractCreationTwenty = CreationEntryCreator.createAdvancedEntry(SkillList.PAPYRUSMAKING, ItemList.paperSheet, ItemList.ink, ItemList.merchantContract, true, true, 0.1f, true, false, CreationCategories.WRITING);
                    contractCreationTwenty.addRequirement(new CreationRequirement(1, ItemList.coinSilverTwenty, contractPrice/20, true));
                    logger.info("Added creation entry for twenty-silver coins.");
                }
            } else {
                CreationEntryCreator.createSimpleEntry(SkillList.PAPYRUSMAKING, ItemList.paperSheet, ItemList.ink, ItemList.merchantContract, true, true, 0.1f, true, false, CreationCategories.WRITING);
                logger.info("Added creation entry for zero silver coins.");
            }
        }
        logger.info("Done with onItemTemplatesCreated.");
    }

    @Override
    public void onServerStarted(){
        ModActions.registerAction(new ReloadConfigAction());
    }

    @Override
    public void onPlayerLogin(Player player) {
        soundData.put(player.getWurmId(), new HashMap<>()); // add empty sound data for player
    }

    @Override
    public void onPlayerLogout(Player player) {
        soundData.remove(player.getWurmId()); // remove data about player if present
    }

    public static boolean isNotRebirthable(int creatureId) {
        return getInstance().notRebirthables.contains(creatureId);
    }

    public String getVersion(){
        return version;
    }

    public static CommonTweaks getInstance(){
        return instance;
    }

    public int getReloadGmPower() {
        return reloadGmPower;
    }


    // TODO: add life preserver
    // TODO: add digtocorner action
    // TODO: rare pottery planters give better ql?
    // TODO: lamp/candelabra hanging from ceiling
    // TODO: window flower boxes //more than one flower? //planter pots set in them???
    // TODO: hanging flower baskets like hanging lamps //more than one flower? //planter pots set in them???
    // TODO: Potter's bench
    // TODO: strongwall reinforce tile instead of collapsing it
}
